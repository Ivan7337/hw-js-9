/*
	1. Сoздать новый HTML тег можно с помощью createElement('tag')
		пример: const newTag = document.createElement('div')

	2. elem.insertAdjacentHTML(where, html), перый аргумент отвечает на то куда вставить html по отношению к elem 
		'beforebegin' – вставить html непосредственно перед elem,
		'afterbegin' – вставить html в начало elem,
		'beforeend' – вставить html в конец elem,
		'afterend' – вставить html непосредственно после elem.

	3. elem.remove()
*/


const showItemsList = (arr, parent = document.body) => {
	const ulItem = document.createElement('ul')

	arr.forEach((el) => {
		const liItems = document.createElement('li')
		if(Array.isArray(el)) {
			showItemsList(el, liItems)
		} else {
			liItems.innerText = el;
		}
		ulItem.prepend(liItems)
	})
	parent.prepend(ulItem)
	
} 

showItemsList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])

showItemsList(["Kharkiv", "Kyiv", ["Borispol", "Irpin"], "Odesa", "Lviv", "Dnipro"]);


let count = 4;
const countdown = setInterval(() => {

const text = document.getElementById("timer")
console.log(text)

Object.assign(text.style, {
	color: 'red',
	fontSize: '50px',
	width: '50vw',
	height: '50vh',
	marginLeft: '40%',
})

text.innerHTML = `Осталось: 00:0${count - 1}`

count--;
if (count === 0) {
    clearInterval(countdown);
    document.body.innerHTML = '';
}

}, 1000);